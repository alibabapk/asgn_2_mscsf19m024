#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "\033[1;34mcs525shell@\033[0m"

int pid;
int c;
int execute(char* arglist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
int p=0;
void sigint_handler(int signum){
	kill(pid,9);  
	signal(SIGINT,SIG_DFL);
	system("stty echo");
}
void piping(char* arglist[]){
printf("inside piping()\n");
	int fd[2];
	int rv = pipe(fd);
	if (rv == -1)
	{
	printf("\n Pipe failed");
	exit(1);
	}
	else{
                pid_t p1, p2;
                p1 = fork();
                if (p1==0)
		{             //Here inside child 1
                 close(fd[0]);// no need to take input
                 dup2(fd[1],1); //writing at write end
                 close(fd[1]);
                 execvp(arglist[0],arglist);
             	}else
		{//inside parent here
                  p2 = fork();
                  if(p2==0)
		{//Here inside child 2
                  close(fd[1]);//no need to output so closing it
                  dup2(fd[0],0);//reading from read end
                  execvp(arglist[3], arglist);
		}else{//inside parent to wait for both children
		   wait(NULL);
		   wait(NULL);
		   }
		}
	}
}
int execute_rd(char **arglist){
   printf("inside execute_rd()\n"); 
   char buff[1024];
   int status;
   pid = fork();
   switch(pid){
      case -1:
         perror("fork failed");
              exit(1);
      case 0:
//		FILE *fp;
                if (*arglist[1]=='<' && *arglist[3]=='>')
                {
                        FILE *fpr = fopen(arglist[2],"r");
			int fdr = read(1,buff,1024);
                        dup2(fdr,0);
                       
			FILE *fpw = fopen(arglist[4],"w");
			int fdw = write(0,buff,1024);
                        dup2(fdw,1);
                }
                else if (*arglist[1]=='>' && *arglist[3]=='<')
                {
                        FILE *fpr = fopen(arglist[4],"r");
                        int fdr = read(1,buff,1024);
                        dup2(fdr,0);

			FILE *fpw = fopen(arglist[2],"w");
			int fdw = write(0,buff,1024);
                        dup2(fdw,1);
		}
		else
                //execvp(arglist[0], arglist);
                perror("Command not found...");
                exit(1);
      default:
              waitpid(pid, &status, 0);
              // printf("child exited with status %d \n", status >> 8);
         return 0;
   }

}

int main(){
   char *cmdline;
   char** arglist;
   char* prompt = PROMPT;   
   signal(SIGINT,sigint_handler);

   while((cmdline = read_cmd(prompt,stdin)) != NULL){
      if((arglist = tokenize(cmdline)) != NULL){
       // if (arglist[1][0]=='>' || arglist[1][0]=='<' || arglist[3][0] == '<' || arglist[3][0] == '>')    
	// execute_rd(arglist);
	//else if (arglist[1][0] == '|' || arglist[2][0] == '|' || arglist[3][0])
	//piping(arglist);
	//else
	 execute(arglist); 
       //  need to free arglist
         for(int j=0; j < MAXARGS+1; j++)
             free(arglist[j]);
         free(arglist);
         free(cmdline);
      }
  }//end of while loop

printf("\n");
   return 0;
}
int execute(char* arglist[]){
   printf("inside execute()\n");
   int status;
   pid = fork();
   switch(pid){
      case -1:
         perror("fork failed");
              exit(1);
      case 0:
		execvp(arglist[0], arglist);
                perror("Command not found...");
                exit(1);
      default:
	      waitpid(pid, &status, 0);
              // printf("child exited with status %d \n", status >> 8);
         return 0;
   }
}
char** tokenize(char* cmdline){

 if(cmdline[0] == '\0')//if user has entered nothing and pressed enter key. Writing it before the for loop of allocating
      return NULL;     //memory is better in a way that it returns NULL and no need to allocate memory.

//allocate memory
   char** arglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
   for(int j=0; j < MAXARGS+1; j++){
           arglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
      bzero(arglist[j],ARGLEN);
    }

   int argnum = 0; //slots used
   char*cp = cmdline; // pos in string
   char*start;
   int len;
   while(*cp != '\0'){
      while(*cp == ' ' || *cp == '\t') //skip leading spaces
          cp++;
      start = cp; //start of the word
      len = 1;
      //find the end of the word
      while(*++cp != '\0' && !(*cp ==' ' || *cp == '\t'))
         len++;
      strncpy(arglist[argnum], start, len);
      arglist[argnum][len] = '\0';
      argnum++;
   }
   arglist[argnum] = NULL;
   return arglist;
}

char* read_cmd(char* prompt, FILE* fp){
   printf("%s", prompt);
  //int c; //input character
 int pos = 0; //position of character in cmdline
   char* cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
   while((c = getc(fp)) != EOF){
       if(c == '\n')
          break;
       cmdline[pos++] = c;
   }
//these two lines are added, in case user press ctrl+d to exit the shell
   if(c == EOF && pos == 0) 
      return NULL;
   cmdline[pos] = '\0';
   return cmdline;
}



