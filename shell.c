#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
int execute(char**);
int mysystem(char*);
extern char ** environ;
void exit_handler(){
   printf("Exit handler\n");
}
int main(){
pid_t cpid1 = fork();
   if (cpid1 == 0){
      printf("Running child, PID=%ld PPID=%ld\n",(long)getpid(), (long)getppid());
      while(1);
   }
   else{
      printf("Terminating parent, PID=%ld PPID=%ld\n",(long)getpid(), (long)getppid());
      exit(0);
   }

   atexit(exit_handler);
   mysystem("ls -l /home");
   pid_t cpid2 = fork();
   if (cpid2 == 0){
   execlp("/bin/ls", "myls", "/home", '\0');
      printf("This is child process.\n");
      exit(0);
   }
   else{
      wait(NULL);
      printf("This is parent process.\n");
      exit(0);
   }
   int status;
   pid_t cpid3 = fork();
   if (cpid3 == 0){
      char *argv[]={"myls","-l","/home",'\0'};
      execv("/bin/ls",argv);
      perror("exec failed");   
  }
   else{
      wait(&status);
      printf("Hello I m Parent.\n");
   }
char	*arglist[10];
   arglist[0] = "/bin/ls";
   arglist[1] = "-l";
   arglist[2] = "/home";
   arglist[2] = NULL;
   int rv = execute(arglist);
   return rv;
}
int mysystem(char * cmd){
   pid_t cpid = fork();
   switch(cpid){
      case -1:
	      perror("fork failed");
	      return -1;
      case 0:
	      execlp("/bin/bash", "mybash","-c", cmd, '\0');
	      perror("execlp() failed");
         return -2;
      default:
	      waitpid(cpid, NULL, 0);
	      return 0;
   }
}
int execute(char *arglist[]){
   int status;
   pid_t cpid = fork();
   switch(cpid){
      case -1:
         perror("fork failed");
         exit(1);
      case 0:
         execvp(arglist[0], arglist);
         perror("execvp failed");
	      exit(1);
      default:
	      waitpid(cpid, &status, 0);
         status = status >> 8;
         printf("Child exited with status %d\n", status);
         return status;
   }
}
